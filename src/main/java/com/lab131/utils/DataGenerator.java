package com.lab131.utils;

import java.util.ArrayList;
import java.util.Random;

public class DataGenerator {

    private static ArrayList<String> fnames = new ArrayList<String>();
    private static ArrayList<String> lnames = new ArrayList<String>();
    private static ArrayList<String> companies = new ArrayList<String>();
	private static ArrayList<String> cities = new ArrayList<String>();
	private static ArrayList<String> streetNames = new ArrayList<String>();
	private static ArrayList<String> stateAbbreviations = new ArrayList<String>();
    private static Random generator = new Random(System.currentTimeMillis());
    private static boolean isDataSetup = false;

    public static void setupData() {
        fnames = new ArrayList<String>();
        lnames = new ArrayList<String>();
        companies = new ArrayList<String>();

        fnames.add("Priscella");
        fnames.add("Jon");
        fnames.add("Ethan");
        fnames.add("Noah");
        fnames.add("Cayden");
        fnames.add("Landon");
        fnames.add("Owen");
        fnames.add("Grace");
        fnames.add("Audrey");
        fnames.add("Ella"); //10
        fnames.add("Chloe");
        fnames.add("Elvis");
        fnames.add("Scooby");
        fnames.add("Fred");
        fnames.add("Dafne");
        fnames.add("Shaggy");
        fnames.add("Sofia");
        fnames.add("Abigail");
        fnames.add("Jackson");
        fnames.add("Gavin"); //20
        fnames.add("Connor");
        fnames.add("Caleb");
        fnames.add("Madelyn");
        fnames.add("Bethany");
        fnames.add("Charlotte");
        fnames.add("Natalie");
        fnames.add("Alyssa");
        fnames.add("Aaron");

        lnames.add("Johnson");
        lnames.add("Smith");
        lnames.add("Jones");
        lnames.add("Taylor");
        lnames.add("Anderson");
        lnames.add("Thomas");
        lnames.add("Jackson");
        lnames.add("White");
        lnames.add("Harris");
        lnames.add("Martin"); //10
        lnames.add("Thompson");
        lnames.add("Garcia");
        lnames.add("Martinez");
        lnames.add("Robinson");
        lnames.add("Clark");
        lnames.add("Doo");
        lnames.add("Rodriguez");
        lnames.add("Lewis");
        lnames.add("Lee");
        lnames.add("Walker"); //20
        lnames.add("Hall");
        lnames.add("Allen");
        lnames.add("Young");
        lnames.add("Hernandez");
        lnames.add("King");
        lnames.add("Wright");
        lnames.add("Lopez");
        lnames.add("Hill");

        companies.add("NetApp");
        companies.add("Edward Jones");
        companies.add("Boston Consulting Group");
        companies.add("Google");
        companies.add("Wegmans Food Markets");
        companies.add("Initech");
        companies.add("Genentech");
        companies.add("Methodist Hospital System");
        companies.add("Nugget Market");
        companies.add("Adobe Systems");
        companies.add("Goldman Sachs");
        companies.add("Devon Energy");//10
        companies.add("Qualcomm");
        companies.add("Boise State University");
        companies.add("WinCo Foods");
        companies.add("Zappos.com");
        companies.add("Starbucks");
        companies.add("ShopKo");
        companies.add("NuStar Energy");
		companies.add("Tesla");
		companies.add("Lab131");

		streetNames.add("Roosevelt Avenue");
		streetNames.add("3rd Street North");
		streetNames.add("York Road");
		streetNames.add("Inverness Drive");
		streetNames.add("Lantern Lane");
		streetNames.add("Sherwood Drive");
		streetNames.add("Locust Street");
		streetNames.add("Ridge Avenue");
		streetNames.add("Cedar Avenue");
		streetNames.add("5th Street East");
		streetNames.add("Route 27");
		streetNames.add("King Street");
		streetNames.add("Depot Street");
		streetNames.add("Canal Street");
		streetNames.add("North Avenue");
		streetNames.add("Route 44");
		streetNames.add("William Street");
		streetNames.add("Front Street North");
		streetNames.add("Cleveland Avenue");
		streetNames.add("Devonshire Drive");
		streetNames.add("Division Street");
		streetNames.add("State Street");
		streetNames.add("Wood Street");
		streetNames.add("12th Street");
		streetNames.add("Broadway");
		streetNames.add("Glenwood Avenue");
		streetNames.add("Franklin Street");
		streetNames.add("Route 20");
		streetNames.add("Broad Street West");
		streetNames.add("Elm Avenue");
		streetNames.add("Myrtle Avenue");
		streetNames.add("Country Lane");
		streetNames.add("Cardinal Drive");
		streetNames.add("Washington Avenue");
		streetNames.add("Hickory Street");
		streetNames.add("Orchard Lane");
		streetNames.add("Ashley Court");
		streetNames.add("Colonial Drive");
		streetNames.add("Lincoln Street");
		streetNames.add("Market Street");

		cities.add("Henderson");
		cities.add("Philadelphia");
		cities.add("San Jose");
		cities.add("Reno");
		cities.add("Glendale");
		cities.add("Albuquerque");
		cities.add("Pittsburgh");
		cities.add("Seattle");
		cities.add("Buffalo");
		cities.add("Modesto");
		cities.add("Jacksonville");
		cities.add("Jersey City");
		cities.add("Chula Vista");
		cities.add("Arlington");
		cities.add("Corpus Christi");
		cities.add("Fort Wayne");
		cities.add("Long Beach");
		cities.add("Rochester");
		cities.add("Tampa");
		cities.add("Garland");
		cities.add("San Diego");
		cities.add("Charlotte");
		cities.add("Fort Worth");
		cities.add("Plano");
		cities.add("Mesa");
		cities.add("Billings");
		cities.add("Walker");
		cities.add("Nevis");
		cities.add("Bismarck");
		cities.add("Cody");

		stateAbbreviations.add("AL");
		stateAbbreviations.add("AK");
		stateAbbreviations.add("AS");
		stateAbbreviations.add("AZ");
		stateAbbreviations.add("AR");
		stateAbbreviations.add("CA");
		stateAbbreviations.add("CO");
		stateAbbreviations.add("CT");
		stateAbbreviations.add("DE");
		stateAbbreviations.add("DC");
		stateAbbreviations.add("FL");
		stateAbbreviations.add("GA");
		stateAbbreviations.add("GU");
		stateAbbreviations.add("HI");
		stateAbbreviations.add("ID");
		stateAbbreviations.add("IL");
		stateAbbreviations.add("IN");
		stateAbbreviations.add("IA");
		stateAbbreviations.add("KS");
		stateAbbreviations.add("KY");
		stateAbbreviations.add("LA");
		stateAbbreviations.add("ME");
		stateAbbreviations.add("MD");
		stateAbbreviations.add("MH");
		stateAbbreviations.add("MA");
		stateAbbreviations.add("MI");
		stateAbbreviations.add("FM");
		stateAbbreviations.add("MN");
		stateAbbreviations.add("MS");
		stateAbbreviations.add("MO");
		stateAbbreviations.add("MT");
		stateAbbreviations.add("NE");
		stateAbbreviations.add("NV");
		stateAbbreviations.add("NH");
		stateAbbreviations.add("NJ");
		stateAbbreviations.add("NM");
		stateAbbreviations.add("NY");
		stateAbbreviations.add("NC");
		stateAbbreviations.add("ND");
		stateAbbreviations.add("MP");
		stateAbbreviations.add("OH");
		stateAbbreviations.add("OK");
		stateAbbreviations.add("OR");
		stateAbbreviations.add("PW");
		stateAbbreviations.add("PA");
		stateAbbreviations.add("PR");
		stateAbbreviations.add("RI");
		stateAbbreviations.add("SC");
		stateAbbreviations.add("SD");
		stateAbbreviations.add("TN");
		stateAbbreviations.add("TX");
		stateAbbreviations.add("UT");
		stateAbbreviations.add("VT");
		stateAbbreviations.add("VA");
		stateAbbreviations.add("VI");
		stateAbbreviations.add("WA");
		stateAbbreviations.add("WV");
		stateAbbreviations.add("WI");
		stateAbbreviations.add("WY");

        isDataSetup = true;
    }

	public static String generateAddressLine() {
		if (!isDataSetup) {
			setupData();
		}
		String addrLine = DataGenerator.generateMulidigitNumber(0,9,DataGenerator.generateNumber(1,6)) + " "+DataGenerator.generateStreetName();
		return addrLine;
	}

	public static String generateCity() {
		if (!isDataSetup) {
			setupData();
		}

		int index = DataGenerator.generateNumber(0, cities.size() - 1);
		return cities.get(index).toString();
	}

	public static String generateStateAbbreviation() {
		if (!isDataSetup) {
			setupData();
		}

		int index = DataGenerator.generateNumber(0, stateAbbreviations.size() - 1);
		return stateAbbreviations.get(index).toString();
	}

	public static String generateStreetName() {
		if (!isDataSetup) {
			setupData();
		}

		int index = DataGenerator.generateNumber(0, streetNames.size() - 1);
		return streetNames.get(index).toString();
	}

    public static String generateFirstName() {
        if (!isDataSetup) {
            setupData();
        }

        int index = DataGenerator.generateNumber(0, fnames.size() - 1);
        return fnames.get(index).toString();
    }

    public static String generateLastName() {
        if (!isDataSetup) {
            setupData();
        }

        int index = DataGenerator.generateNumber(0, lnames.size() - 1);
        return lnames.get(index).toString();

    }

    public static String generateCompanyName() {
        if (!isDataSetup) {
            setupData();
        }

        int index = DataGenerator.generateNumber(0, companies.size() - 1);
        return companies.get(index).toString();
    }

    public static String generateEmail(String fname, String lname, String company) {
        if (!isDataSetup) {
            setupData();
        }

        String companyModName = company.replace(" ", "");
        companyModName.toLowerCase();
        StringBuffer sb = new StringBuffer();
        if (generator.nextBoolean()) //first of first, last name plus company
        {
            sb.append(fname.substring(0, 1));
            sb.append(lname);
            sb.append("@");
            sb.append(companyModName);
            sb.append("z28.com");
        } else //first and last name plus company
        {
            sb.append(fname);
            sb.append(lname);
            sb.append("@");
            sb.append(companyModName);
            sb.append("z28.com");
        }
        return sb.toString();
    }

    public static String generatePhoneNumber() {
        if (!isDataSetup) {
            setupData();
        }
        ArrayList<String> n = new ArrayList<String>();

        int a = generateNumber(1, 9);
        int b = generateNumber(0, 9);
        int c = generateNumber(0, 9);
        int d = generateNumber(0, 9);
        int e = generateNumber(0, 9);
        int f = generateNumber(0, 9);
        int g = generateNumber(0, 9);
        int h = generateNumber(0, 9);
        int i = generateNumber(0, 9);
        int j = generateNumber(0, 9);
        int k = generateNumber(0, 9);

        return "" + a + b + c + "." + d + e + f + "." + h + i + j + k;

    }

    public static String generatePosition() {
        if (!isDataSetup) {
            setupData();
        }
        ArrayList<String> n = new ArrayList<String>();

        n.add("Front Desk");
        n.add("Human Resources");
        n.add("Sales Executive");
        n.add("Electrical Engineer");
        n.add("Software Developer");
        n.add("Welder");
        n.add("Architect");
        n.add("Construction");
        n.add("Support");

        int index = DataGenerator.generateNumber(0, n.size() - 1);
        return n.get(index).toString();
    }

    public static int generateNumber(int min, int max) {

//				var scale:Number = max - min;
//	        		var ret:int = Math.round(Math.random() * scale + min);
        return generator.nextInt(max);
    }

	public static String generateMulidigitNumber(int min, int max, int count) {
		StringBuilder multiDigitNumber = new StringBuilder();
		for(int i=0; i<count; i++) {
			multiDigitNumber.append(generateNumber(0,9));
		}
		return multiDigitNumber.toString();
	}

//			public static function testOutput():String
//			{
//				
//				var fname:String = DataGeneratorUtility.generateFirstName();
//				var lname:String = DataGeneratorUtility.generateLastName();
//				var companyName:String = DataGeneratorUtility.generateCompanyName();
//				var email:String = DataGeneratorUtility.generateEmail(fname,lname,companyName);
//				var age:int = DataGeneratorUtility.generateNumber(20,30);
//				var ret:String = fname+" "+lname+" "+companyName+" "+email+" "+age;
//				trace(ret);
//				return ret;
//				
//			}
    /**
     *
     * @param args
     */
    public static void showSample() {

        int count = 500;

        System.out.println("Starting generating " + count + " first and last names.");
        ArrayList<String> n = new ArrayList<String>(count);

        String fullLine;
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            fullLine = DataGenerator.generateFirstName() + " " + DataGenerator.generateLastName() +"\t\t("+DataGenerator.generatePosition()+" at "+DataGenerator.generateCompanyName()+") - "+
					DataGenerator.generateMulidigitNumber(0,9,DataGenerator.generateNumber(1,6)) + " "+DataGenerator.generateStreetName()+" "+DataGenerator.generateCity()+" "+
					DataGenerator.generateMulidigitNumber(0,9,5)+" "+DataGenerator.generateStateAbbreviation();
            n.add(fullLine);
        }
        long finishTime = System.currentTimeMillis();
        System.out.println("Finished.");
		long totalTime = finishTime - startTime;

        for (int i = 0; i < count; i++) {
            System.out.println(n.get(i));
        }

        System.out.println("Time generating " + count + " first and last names: " + totalTime + " milliseconds (1000 ms = 1 sec)");

    }

	public static boolean isVoidString(String testString) {
		if(testString==null||testString.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	public static String fillStringElement(String elementToTry, String elementToUseOnFail) {
		if(isVoidString(elementToTry)) {
			return elementToUseOnFail;
		} else {
			return elementToTry;
		}
	}
}
