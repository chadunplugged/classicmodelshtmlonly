package com.lab131.repositories;

import com.lab131.entities.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by chadj on 12/22/15.
 */
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    Page<Customer> findAllCustomersByCustomerNumberNotNull(Pageable pageable);
}
