package com.lab131.controllers.html;

import com.lab131.entities.Customer;
import com.lab131.repositories.CustomerRepository;
import com.lab131.service.UrlHelperSingleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by chadj on 12/22/15.
 */
@Controller
public class CustomerControllerHTML {

	private static Logger LOG = LoggerFactory.getLogger(CustomerControllerHTML.class);

	private final CustomerRepository customerRepository;

	@Autowired
	public CustomerControllerHTML(final CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@RequestMapping(value = "customers/{customerNumber}", produces = "text/html")
	public String findOneCustomer(
			@PathVariable(value = "customerNumber") final int customerNumber,
			Model model) {

		final Customer customer = customerRepository.findOne(customerNumber);
		model.addAttribute(customer);
		return "customerDetails";
	}

	@RequestMapping(value="/customers", method=RequestMethod.POST, produces = "text/html")
	public String processSubmit(HttpServletRequest request,
								HttpServletResponse response) {
		// TODO Add input validation
		final HttpHeaders headers = new HttpHeaders();
		return "customerList";
	}

	@RequestMapping(value="/customers", produces = "text/html")
	public String findAllCustomersHTML(
			Model model, Pageable pageable) {

		Page<Customer> customersPage = customerRepository.findAllCustomersByCustomerNumberNotNull(pageable);
		List<Customer> customerList = customersPage.getContent();


		model.addAttribute(customerList);
		model.addAttribute("baseUrl", UrlHelperSingleton.baseurl);
		model.addAttribute("currentPage",pageable.getPageNumber());
		if(customersPage.hasNext()) {
			model.addAttribute("nextPage", UrlHelperSingleton.baseurl + "/customers/?page=" + (pageable.getPageNumber() + 1));
		}
		if(customersPage.hasPrevious()) {
			model.addAttribute("prevPage", UrlHelperSingleton.baseurl + "/customers/?page=" + (pageable.getPageNumber() - 1));
		}
		return "customersList";
	}
}

